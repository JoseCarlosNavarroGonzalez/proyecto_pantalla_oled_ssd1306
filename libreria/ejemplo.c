#include "interfaz_oled.c"

int main(){
	int ac;
	
	printf("Introduzca 0 para cerrar\n");
	printf("Introduzca 1 para hacer prueba de escritura\n");
	printf("Introduzca 2 para hacer prueba de borrado\n");
	printf("Accion: ");
	scanf("%d", &ac);
	
	while(ac != 0){
		if(ac == 1){
			oled_printf("Numero: %d\n", 12);
			oled_printf("Caracter: %c\n", 'Z');
			oled_printf("Cadena: %s\n","prueba");
		}else if (ac == 2){
			oled_clean();
		}
		printf("Introduzca 0 para cerrar\n");
		printf("Introduzca 1 para hacer prueba de escritura\n");
		printf("Introduzca 2 para hacer prueba de borrado\n");
		printf("Accion: ");
		scanf("%d", &ac);
	}

}
