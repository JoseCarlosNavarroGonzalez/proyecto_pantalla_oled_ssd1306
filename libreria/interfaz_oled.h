#ifndef __inferfaz_oled__
#define __inferfaz_oled__

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include "string.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


/* For ioctl*/
#define BORRAR  0x6301

int oled_printf (const char *formato, ...);
 
void oled_clean(void);

#endif
