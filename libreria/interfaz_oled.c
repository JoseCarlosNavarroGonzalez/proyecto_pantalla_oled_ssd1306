#include "interfaz_oled.h"

int oled_printf (const char *formato, ...){
	
	va_list args;
	int res;
	FILE *fp;
	
	
	fp = fopen("/dev/ssd1306_oled", "r+" );
	
	if (fp == NULL){
		printf("Error no existe el fichero que se desea escribir \n");
		exit (1);
	}

	va_start (args, formato);

	res = vfprintf (fp, formato, args);

	va_end (args);
	
	fclose(fp);

	return res;
   
 }
 
 void oled_clean(){
	
	int res, fp;
	
	fp = open("/dev/ssd1306_oled", O_RDWR);
	
	if (fp == -1){
		printf("Error no existe el fichero que se desea escribir \n");
		exit (1);
	}else{
		res = ioctl(fp, BORRAR);
		if (res != 0){
			printf("Error al borrar pantalla \n");	
		}
	}
	res = close(fp);
	
	if(fp == -1){
		 exit (1);
	}
	
}
