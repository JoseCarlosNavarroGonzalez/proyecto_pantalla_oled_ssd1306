/* -*- mode: c; c-file-style: "linux" -*- */
/*
 * I2C Driver for oled SSD1306 over I2C
 *
 * 2018 José Carlos Navarro González.
 * 
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <asm/uaccess.h>      
#include <asm/ioctl.h>
#include "ssd1306_oled.h"

/**********************************************************************/
/* Globals                                                            */
/**********************************************************************/
/* defines from: https://github.com/adafruit/Adafruit_SSD1306*/

static struct ssd1306_oled *my_oled;

#define SSD1306_128_64

#define SSD1306_I2C_ADDRESS   0x3C  

#define SSD1306_SETCONTRAST 0x81
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYALLON 0xA5
#define SSD1306_NORMALDISPLAY 0xA6
#define SSD1306_INVERTDISPLAY 0xA7
#define SSD1306_DISPLAYOFF 0xAE
#define SSD1306_DISPLAYON 0xAF

#define SSD1306_SETDISPLAYOFFSET 0xD3
#define SSD1306_SETCOMPINS 0xDA

#define SSD1306_SETVCOMDETECT 0xDB

#define SSD1306_SETDISPLAYCLOCKDIV 0xD5
#define SSD1306_SETPRECHARGE 0xD9

#define SSD1306_SETMULTIPLEX 0xA8

#define SSD1306_SETLOWCOLUMN 0x00
#define SSD1306_SETHIGHCOLUMN 0x10

#define SSD1306_SETSTARTLINE 0x40

#define SSD1306_MEMORYMODE 0x20
#define SSD1306_COLUMNADDR 0x21
#define SSD1306_PAGEADDR   0x22

#define SSD1306_COMSCANINC 0xC0
#define SSD1306_COMSCANDEC 0xC8

#define SSD1306_SEGREMAP 0xA0

#define SSD1306_CHARGEPUMP 0x8D

#define SSD1306_EXTERNALVCC 0x1
#define SSD1306_SWITCHCAPVCC 0x2

//define ioctl
#define SSD1306_NUM_MAG 'c'

#define SSD1306_BORRAR _IO(SSD1306_NUM_MAG, 1)

#define SSD1306_NUM_MAX 2


// Scrolling #defines
#define SSD1306_ACTIVATE_SCROLL 0x2F
#define SSD1306_DEACTIVATE_SCROLL 0x2E
#define SSD1306_SET_VERTICAL_SCROLL_AREA 0xA3
#define SSD1306_RIGHT_HORIZONTAL_SCROLL 0x26
#define SSD1306_LEFT_HORIZONTAL_SCROLL 0x27
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A


/**********************************************************************/
/* Set frame buffer to zero                                           */
/**********************************************************************/
static void clear_buffer(void){
	
	memset(buffer, 0, (SSD1306_LCDWIDTH*SSD1306_LCDHEIGHT/8));
}

/**********************************************************************/
/* Write data byte througth i2c client                                */
/**********************************************************************/
static void ssd1306_command(uint8_t value){
	
    uint8_t command = 0x00;   // Co = 0, D/C = 0  
    i2c_smbus_write_byte_data(my_oled->my_client, command, value);   
}
 
/**********************************************************************/
/* Write data block througth i2c client							      */
/**********************************************************************/
static void ssd1306_writedatablock(uint8_t * values, uint8_t length){ 
 
    uint8_t command = 0x40;   // Co = 0, D/C = 1  
    i2c_smbus_write_i2c_block_data(my_oled->my_client, 
								   command, 
								   length, 
								   values);   
}

/**********************************************************************/
/* Write frame buffer to display                                      */
/**********************************************************************/
static void display(void){
	
  uint16_t i;
  
  ssd1306_command(SSD1306_COLUMNADDR);
  ssd1306_command(0);   // Column start address (0 = reset)
  ssd1306_command(SSD1306_LCDWIDTH-1); // Column end address (127 = reset)

  ssd1306_command(SSD1306_PAGEADDR);
  ssd1306_command(0); // Page start address (0 = reset)
  ssd1306_command(SSD1306_LCDHEIGHT/8 - 1); // Page end address

  // I2C
  for (i=0; i<(SSD1306_LCDWIDTH*SSD1306_LCDHEIGHT/8); i+=16){
	  ssd1306_writedatablock(buffer+i,16);
  }
}


/**********************************************************************/
/* Display initialization											  */
/**********************************************************************/
static void init_sequence(uint8_t vccstate){ // vccstate ?

  // Init sequence
  ssd1306_command(SSD1306_DISPLAYOFF);                  // 0xAE
  ssd1306_command(SSD1306_SETDISPLAYCLOCKDIV);          // 0xD5
  ssd1306_command(0x80);                     // the suggested ratio 0x80

  ssd1306_command(SSD1306_SETMULTIPLEX);                  // 0xA8
  ssd1306_command(SSD1306_LCDHEIGHT - 1);

  ssd1306_command(SSD1306_SETDISPLAYOFFSET);              // 0xD3
  ssd1306_command(0x0);                                   // no offset
  ssd1306_command(SSD1306_SETSTARTLINE | 0x0);            // line #0
  ssd1306_command(SSD1306_CHARGEPUMP);
                      
  if (vccstate == SSD1306_EXTERNALVCC){
     ssd1306_command(0x10); 
  }else{
     ssd1306_command(0x14); 
  }
  
  ssd1306_command(SSD1306_MEMORYMODE);                    // 0x20
  ssd1306_command(0x00);                         // 0x0 act like ks0108
  ssd1306_command(SSD1306_SEGREMAP | 0x1);
  ssd1306_command(SSD1306_COMSCANDEC);

  //-- SSD1306_128_64
  ssd1306_command(SSD1306_SETCOMPINS);                    // 0xDA
  ssd1306_command(0x12);
  ssd1306_command(SSD1306_SETCONTRAST);                   // 0x81
  
  if (vccstate == SSD1306_EXTERNALVCC){
     ssd1306_command(0x9F); 
  }else{
     ssd1306_command(0xCF); 
  }
  
  ssd1306_command(SSD1306_SETPRECHARGE);                  // 0xd9
  
  if (vccstate == SSD1306_EXTERNALVCC){
     ssd1306_command(0x22); 
  }else{
     ssd1306_command(0xF1); 
  }
  
  ssd1306_command(SSD1306_SETVCOMDETECT);                 // 0xDB
  ssd1306_command(0x40);
  ssd1306_command(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
  ssd1306_command(SSD1306_NORMALDISPLAY);                 // 0xA6

  ssd1306_command(SSD1306_DEACTIVATE_SCROLL);

  ssd1306_command(SSD1306_DISPLAYON);
}




/**********************************************************************/
/* Function for display  											  */
/**********************************************************************/
static int xpos, ypos;

void printchar(char c){

	int j;
	for (j=0; j<8 ; j++){
		
		buffer[ypos*16*8+xpos*8+j]=font[c*8+j];
    }
}

void printoled(char *buf, int n){

	int i;
	for (i=0; i<n; i++){
	
		if(buf[i]=='\n'){
		
			xpos=0; ypos++; 
			
		}else{
		
			printchar(buf[i]);
			xpos++;
			
			if(xpos==16){
				
				xpos=0; 
				ypos++;
				
			}
		}
		
		if(ypos==8){
		
			memcpy(buffer, buffer+16*8, 16*8*7);
			memset(buffer+16*8*7, 0, 16*8);
			ypos=7;
		}
		
	}
}


/**********************************************************************/
/* Device file operations                                             */
/**********************************************************************/
static ssize_t ssd1306_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *ppos){

    char cadena[64];
	
	if(count>64){
		 count=64;
	}
	
    if (copy_from_user( &cadena, buf, count )) {
        return -EFAULT;
    }
    

	printoled (cadena,count);
	display();
	
	*ppos+=count;
    return count;
}

static long ssd1306_ioctl(struct file *file, unsigned int cmd, 
							unsigned long args){
	
		if (_IOC_TYPE(cmd) != SSD1306_NUM_MAG){
			return -EINVAL;
        }
         
        if (_IOC_NR(cmd) > SSD1306_NUM_MAX){ 
			return -EINVAL;
		}
			
	
	switch(cmd){
		
		case SSD1306_BORRAR:
			printk(KERN_INFO "(ioctl %d) borrada pantalla\n", 
					_IOC_NR(cmd));
					
			xpos=0;
			ypos=0;
			clear_buffer();
			printoled("-PANTALLA OLED-\n",16);
			display();
		break;
		
		default:
			return -EINVAL;
		
	}

		return 0;
}


static const struct file_operations oled_fops = {
    .owner	= THIS_MODULE,
    .write	= ssd1306_write,
    .unlocked_ioctl	= ssd1306_ioctl,
};


/**********************************************************************/
/* I2C detection                                                      */
/**********************************************************************/
struct ssd1306_oled *ssd1306_i2c_register_hardware(struct device *dev,
                                            struct i2c_client *client){

	struct ssd1306_oled *oled;

    if ((oled = kzalloc(sizeof(*oled), GFP_KERNEL)) == NULL){
		return NULL;
	}
          
    printk(KERN_NOTICE "Hello, loading DSO %s module!\n",
			KBUILD_MODNAME);
    strcpy(oled->devname, "ssd1306_oled");        
    oled->dev = get_device(dev);
    dev_set_drvdata(dev, oled);
        
    oled->miscdev.minor = MISC_DYNAMIC_MINOR;
    oled->miscdev.fops = &oled_fops;
    oled->miscdev.name = oled->devname;
    oled->miscdev.mode = S_IRUGO | S_IWUGO	;
       
    oled->my_client = client;

    if (ssd1306_i2c_add_device(oled)){
        put_device(oled->dev);
		kfree(oled);
    }


    return oled;
       
}

int ssd1306_probe(struct i2c_client *client,
                           const struct i2c_device_id *id){
	
	
    struct device *dev = &client->dev;
	struct ssd1306_oled *oled;
	
	xpos=0;
	ypos=0;
	
	if(!i2c_check_functionality(client->adapter, 
			I2C_FUNC_SMBUS_BYTE_DATA )){ 
				
		printk(KERN_ERR "Needed i2c functionality is not supported\n");
		return -ENODEV;
	}
	
	
	if ((oled = ssd1306_i2c_register_hardware(dev, client)) == NULL){
		return -ENODEV;
    }else {
		my_oled = oled;
    }
     
	printk(KERN_NOTICE "Display initialization\n");
	init_sequence(SSD1306_SWITCHCAPVCC); 

	clear_buffer();
	printoled("-PANTALLA OLED-\n",16);
	printk(KERN_NOTICE "Write buffer to display...\n");
	display(); 
     	
    return 0;
				   							   
}

int ssd1306_i2c_add_device(struct ssd1306_oled  *oled){
	
    int ret;
       

    if ((ret = misc_register(&oled->miscdev)) != 0){
		oled->miscdev.name = NULL;
        dev_err(oled->dev,
                "unable to misc_register %s, minor %d err=%d\n",
                 oled->miscdev.name,
                 oled->miscdev.minor,
                 ret);
    }


    return ret;
}


int ssd1306_remove(struct i2c_client *client){
	
	
	printk(KERN_NOTICE "%s module cleaning up...\n",KBUILD_MODNAME);
	printk(KERN_NOTICE "Unregistering i2c client\n");
	printk(KERN_NOTICE "Clearing buffer...\n");
	clear_buffer();
	printk(KERN_NOTICE "Write buffer to display...\n");
	display();
		
		
	if(&my_oled->miscdev.this_device){
		misc_deregister(&my_oled->miscdev);
	}
			
	kfree(my_oled);
	
	printk(KERN_NOTICE "Done. Bye\n");

	return 0;	
}


MODULE_DEVICE_TABLE(i2c, ssd1306_i2c_id);

static struct i2c_driver ssd1306_i2c_driver  = { 
	.driver = {
		.owner = THIS_MODULE,
		.name  = "ssd1306_oled",
	},
	
	.probe    = ssd1306_probe,
	.remove   = ssd1306_remove,
	.id_table = ssd1306_i2c_id,
	
};


/**********************************************************************/
/* Modules init/cleanup                                               */
/**********************************************************************/
static int __init  ssd1306_i2c_init(void){
	
	return i2c_add_driver(&ssd1306_i2c_driver);
	
}

static void __exit ssd1306_i2c_cleanup(void){
	
	i2c_del_driver(&ssd1306_i2c_driver);
    
}


module_init(ssd1306_i2c_init);
module_exit(ssd1306_i2c_cleanup);

/**********************************************************************/
/* Modules licensing/description block.                               */
/**********************************************************************/
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jose Carlos Navarro Gonzalez");
MODULE_DESCRIPTION("Modulo para pantalla oled SSD1306 128X64 I2C");

